## Tibu Time Budgets

![](https://i.vgy.me/pao4AT.png)

Use Time Budgets to plan your week & track what you're doing!

features:
- very simple
- set weekly time budgets
- plan the time you want to spend for various tasks
- record the time spent
- let tibu suggest breaks according to the pomodoro technique
- tag your tasks for more fine-grained tracking *(not yet implemented)*
- export your timesheet as a PDF *(not yet implemented)*

tech stack:
- vue 3 + naive ui for the ui
- go + air as the backend *(not yet implemented)*
- server-sent events for live updates *(not yet implemented)*
- service worker for notification api *(not yet implemented)*
